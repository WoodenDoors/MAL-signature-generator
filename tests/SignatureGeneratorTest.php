<?php

use \PHPUnit\Framework\TestCase;
use \WoodenDoors\Mal\SignatureGenerator;
use \WoodenDoors\Mal\SignatureText;

class SignatureGeneratorTest extends TestCase
{

    public function testImage(): void
    {
        $user = "Black_Rock";

        $baseDir = __DIR__ . '/..';
        $cachePath = $baseDir . "/cache/sig.png";
        $font = $baseDir . '/res/TrajanRegular.ttf';
        $bgImage = $baseDir . "/res/SHOUJO_ROSE_4-10_bg.png";
        $overlayImage = $baseDir . "/res/SHOUJO_ROSE_4-10_fg.png";

        $generator = new SignatureGenerator($user, $bgImage, $cachePath, 0);
        $generator->addText(new SignatureText(102, 20, 20, 0, 50, 0, 310, 96,
            $font, $generator->getMalData()->getTitles()[0], 'c'));
        $generator->overlayImage($overlayImage);
        $generator->finalize(false);

        $generatedSize = getimagesize($cachePath);
        $this->assertEquals(600, $generatedSize[0]);
        $this->assertEquals(140, $generatedSize[1]);

        $generatedImage = imagecreatefrompng($cachePath);
        $this->asertPixelColor($generatedImage, 0, 0, ['red' => 12, 'green' => 11, 'blue' => 10]);
        $this->asertPixelColor($generatedImage, 231, 104, ['red' => 208, 'green' => 29, 'blue' => 35]);
        $this->asertPixelColor($generatedImage, 320, 109, ['red' => 77, 'green' => 73, 'blue' => 121]);
    }

    private function asertPixelColor($image, $x, $y, $expectedRgb)
    {
        $actualRgb = imagecolorsforindex($image, imagecolorat($image, $x, $y));
        foreach (['red', 'green', 'blue'] as $color) {
            $this->assertEquals($expectedRgb[$color], $actualRgb[$color],
                "$color value on pixel $x/$y did not match");
        }
    }
}
