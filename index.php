<?php
require __DIR__ . '/vendor/autoload.php';

use WoodenDoors\Mal\SignatureGenerator;
use WoodenDoors\Mal\SignatureText;

$user = "Black_Rock";
$cachePath = __DIR__ . "/cache/sig.gif";
$font = __DIR__ . '/res/TrajanRegular.ttf';
$bgImage = __DIR__ . "/res/ROSE_BG.png";

$generator = new SignatureGenerator($user, $bgImage, $cachePath, 30);

$malData = $generator->getMalData();
$signatureTextTitle = new SignatureText(102, 20, 20, 0, 50, 0, 310, 96, $font,
    $malData->getTitles()[0], 'c');
$signatureTextUnit = new SignatureText(102, 20, 20, 0, 18, 0, 165, 127, $font,
    $malData->getUnits()[0], 'r');
$signatureTextProgress = new SignatureText(102, 20, 20, 0, 18, 0, 440, 127, $font,
    $malData->getCurrent()[0] . "/".$malData->getTotals()[0], 'l');

$generator->addText($signatureTextTitle);
$generator->addText($signatureTextProgress);
$generator->addText($signatureTextUnit);
$generator->overlayImage(__DIR__ . "/res/ROSE_FRAME_1.png");
$generator->saveFrame(__DIR__ . "/cache/sig_frame_1.png", 17);

// got 30 frames initially, but that's far too much, so let's trim it down a little:
for ($i = 4; $i <= 30; $i = $i + 4) {
    $generator->resetImage();
    $generator->addText($signatureTextTitle);
    $generator->addText($signatureTextProgress);
    $generator->addText($signatureTextUnit);
    $generator->overlayImage(__DIR__ . "/res/ROSE_FRAME_$i.png");
    $generator->saveFrame(__DIR__ . "/cache/sig_frame_$i.png", 17);
}

$generator->finalizeAnimated(true);

//$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//error_log("execution time was: $time");
