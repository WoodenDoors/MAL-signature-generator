<?php

namespace WoodenDoors\Mal;

use ErikvdVen\Gif\GIFGenerator;

/************************************************************************
 *  modified version of:
 *
 *  saka's minimal signature script - v1.58 (anime/manga merged)
 *  http://myanimelist.net/forum/?topicid=84446
 *
 *  Modifications include:
 *    - use psr-4 namespaces
 *    - use of a class
 *    - function names
 *    - additional data structures (MalData, SignatureText)
 *    - ability to create animated gifs (mostly thanks to the erikvdven/php-gif dependency)
 *
 ************************************************************************
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
class SignatureGenerator
{
    const CONTENT_TYPE_PNG = "Content-type: image/png";
    const CONTENT_TYPE_GIF = "Content-type: image/gif";

    private $user;
    private $cachePath;
    private $backgroundImage;
    private $image;
    private $malData;
    private $animationFrames;

    /**
     * @param $user
     * @param $backgroundImage
     * @param $cachePath
     * @param $cacheTime
     * @see initialize is automatically called from constructor
     * @see finalize to actually return the image
     */
    public function __construct($user, $backgroundImage, $cachePath, $cacheTime = 10)
    {
        $this->user = $user;
        $this->backgroundImage = $backgroundImage;
        $this->cachePath = $cachePath;
        $this->initialize($cacheTime);
    }

    /**
     * checks the cache and loads from it.
     * if not loads data from MAL (exposed via $this->getMalData()) and prepares the background image.
     *
     * @param int $cacheTime
     */
    private function initialize(int $cacheTime): void
    {
        if ($cacheTime > 0) {
            $this->checkCache($this->cachePath, $cacheTime);
        }

        $animeBuffer = $this->download("https://myanimelist.net/rss.php?type=rw&u={$this->user}");
        $mangaBuffer = $this->download("https://myanimelist.net/rss.php?type=rm&u={$this->user}");
        if (!$animeBuffer or !$mangaBuffer) die("Could not download RSS feed");
        $this->malData = $this->parseMalData($animeBuffer, $mangaBuffer);

        $this->resetImage();
    }

    /**
     * a crude way to reset the image for a new animation frame
     */
    public function resetImage(): void
    {
        if ($this->image) {
            imagedestroy($this->image);
        }
        $this->image = $this->openImage($this->backgroundImage);
    }

    /**
     * @param string $path
     * @param int $delay
     */
    public function saveFrame(string $path, int $delay): void
    {
        $this->animationFrames[] = [
            'image' => $path,
            'delay' => $delay
        ];
        imagepng($this->image, $path);
    }

    /**
     * @param SignatureText $text
     */
    public function addText(SignatureText $text): void
    {
        $color = imagecolorallocatealpha($this->image, $text->getRed(), $text->getGreen(), $text->getBlue(),
            $text->getAlpha());
        $this->writeAlignedTextToImage($this->image, $text->getSize(), $text->getAngle(), $text->getX(),
            $text->getY(), $color, $text->getFont(), $text->getText(), $text->getAlign());
    }

    /**
     * @param bool $outputRawImage
     */
    public function finalize($outputRawImage = true): void
    {
        if ($outputRawImage) {
            header(self::CONTENT_TYPE_PNG);
            imagepng($this->image);
        }
        imagepng($this->image, $this->cachePath);
    }

    /**
     * @param bool $outputRawImage
     */
    public function finalizeAnimated($outputRawImage = true): void
    {
        $gif = (new GIFGenerator())->generate(['frames' => $this->animationFrames]);
        if ($outputRawImage) {
            header(self::CONTENT_TYPE_GIF);
            echo $gif;
        }
        file_put_contents($this->cachePath, $gif);
    }

    /**
     * @param $animeBuffer
     * @param $mangaBuffer
     * @return MalData
     */
    public function parseMalData($animeBuffer, $mangaBuffer): MalData
    {

        // lets fix the status in the manga feeds so they make sense, and so we can differentiate from anime (silly Xinil)
        $mangaBuffer = strtr($mangaBuffer, array(
                '>Plan to Watch' => '>Plan to Read',
                '>Watching' => '>Reading',
                '>Rewatching' => '>Re-Reading',
                '>On-Hold' => '>Reading On Hold',
                '>Completed' => '>Finished Reading')
        );

        // easiest to parse if we just merge the feeds together and parse all at once
        $buffer = $animeBuffer . $mangaBuffer;

        // sanitize the information we saved to the buffer (no newlines/tabs and replace xml entities)
        $buffer = strtr($buffer,
            array(
                "\n" => '',
                "\r" => '',
                "\t" => '',
                '&lt;' => '<',
                '&gt;' => '>',
                '&amp;' => '&',
                '&quot;' => '"',
                '&apos;' => "'"
            )
        );

        // these lines just extract the anime title and status information into $titles[]
        // and $status[] arrays, plus other info
        preg_match_all("/<item><title>([^<]*) - ([^<]*?)<\/title>/i",
            $buffer, $titleMatches);
        preg_match_all("/<description><\!\[CDATA\[([^\]]*) - ([\d?]+) of ([\d?]+) ([^\]]*)\]\]><\/description>/",
            $buffer, $statusMatches);
        preg_match_all("/<pubDate>([^<]*)<\/pubDate>/i", $buffer, $timeMatches);
        preg_match_all("@<link>https?://(?:www\.)?myanimelist\.net/(anime|manga)/(\d+)/[^<]*</link>@i",
            $buffer, $linkMatches);
        $titles = $titleMatches[1]; // $titles is now an array of titles
        $status = $statusMatches[1]; // $status is now an array of statuses
        $current = $statusMatches[2]; // $current is now an array of all the current episodes/chapters
        $totals = $statusMatches[3]; // $totals is now an array of the total number of episodes in each series
        $units = $statusMatches[4]; // $units is now an array of 'episode(s)' or 'chapter(s)'
        $timestamps = $timeMatches[1]; // $timestamps is now an array of dates watched/read
        $types = $linkMatches[1]; // $types is now an array containing the type ('anime' or 'manga')
        $subtypes = $titleMatches[2]; // $subtypes now has the detailed type if you want it ('TV','Movie','Manga','Novel',...)
        $ids = $linkMatches[2]; // $ids is now an array containing the unique numeric mal series id

// sort all of the arrays by the timestamps, so that the most recent entries are first
        $timestamps = array_map('strtotime', $timestamps); // converts to numeric unix timestamps
        array_multisort($timestamps, SORT_DESC, $titles, $status, $current, $totals, $units, $types, $subtypes, $ids);

        for ($i = 0; $i < count($titles); $i++) {
            $titles[$i] = $this->adjustTextToLength($titles[$i], 45);

            $times[$i] = date('D h:ia', $timestamps[$i]);

            $units[$i] = strtr($units[$i], array(
                'episodes' => 'ep.',
                'chapters' => 'ch.',
                'volumes' => 'vol.'
            ));

            $status[$i] = strtr($status[$i], array(
                "Plan to Watch" => "Plan to Watch",
                "Plan to Read" => "Plan to Read",
                "Watching" => "Just watched $units[$i] $current[$i]",
                "Reading" => "Reading at $units[$i] $current[$i]/$totals[$i]",
                "Rewatching" => "Rewatched $units[$i] $current[$i]",
                "Re-Reading" => "Re-reading $units[$i] $current[$i]",
                "On-Hold" => "On Hold at $units[$i] $current[$i]/$totals[$i]",
                "Reading On Hold" => "Reading On Hold",
                "Completed" => "Completed",
                "Finished Reading" => "Finished Reading",
                "Dropped" => "Dropped"
            ));
        }

        $malData = new MalData($titles, $status, $current, $totals, $units, $timestamps, $types, $subtypes, $ids);
        return $malData;
    }

    /**
     * takes any $string you pass it and returns it shortened to $length characters
     * (use it to limit printed string length)
     */
    private function adjustTextToLength($string, $length = 25)
    {
        return (strlen(trim($string)) > $length ? trim(substr(trim($string), 0, $length - 3)) . "..." : $string);
    }

    /**
     * @return string the shortened $string that fits within exactly $pixels width
     * // horizontally when using $font and $size
     */
    private function adjustTextToPixels($string, $pixels, $font, $size)
    {
        for ($k = strlen(trim($string)); $k > 0; $k--) {
            if ($this->calculateBoxTextWidth($this->adjustTextToLength($string, $k), $font, $size) <= $pixels) break;
        }
        return $this->adjustTextToLength($string, $k);
    }

    /**
     * @return mixed the pixel width of the final text with those parameters
     */
    private function calculateBoxTextWidth($string, $font, $size)
    {
        $box = imagettfbbox($size, 0, $font, $string);
        return $box[2] - $box[0];
    }

    /**
     * opens the image at $overlayPath and overlays it onto at position ($x, $y)
     * most image types should work, but 24-bit/true color PNG is recommended if you need transparency
     * @param $overlayPath
     * @param $x
     * @param $y
     */
    public function overlayImage($overlayPath, $x = 0, $y = 0)
    {
        $overlay = is_string($overlayPath) ? $this->openImage($overlayPath) : $overlayPath; //open, or assume opened if non-string
        imagecopy($this->image, $overlay, $x, $y, 0, 0, imagesx($overlay), imagesy($overlay)); // overlay
        @imagedestroy($overlay); // clean up memory, since we don't need the overlay image anymore
    }

    /**
     * will load an image into memory so we can work with it, and die with an error if we fail
     */
    private function openImage($path)
    {
        $image = @imagecreatefromstring(file_get_contents($path));
        if (!$image) die("could not open image ($path) make sure it exists");
        imagealphablending($image, true);
        imagesavealpha($image, true); // preserve transparency
        return $image;
    }

    /**
     * returns a cached image and stops execution if $minutes has not passed since the last update or failure
     */
    private function checkCache($cachePath, $minutes): void
    {
        if (!(is_writable($cachePath) or is_writable(dirname($cachePath)) and !file_exists($cachePath)))
            die("The cache is not writable; please change it to 777 permissions using FTP.\n
            <br />\$cachedpath = {$cachePath}");
        if (
            time() - @filemtime($cachePath) < 60 * $minutes
            and @filemtime(basename($_SERVER['SCRIPT_NAME'])) < @filemtime($cachePath)
        ) {
            if (pathinfo($cachePath, PATHINFO_EXTENSION) === 'gif') {
                header(self::CONTENT_TYPE_GIF);
            } else {
                header(self::CONTENT_TYPE_PNG);
            }
            echo file_get_contents($cachePath);
            exit(0);
        }
        if (file_exists($cachePath)) touch($cachePath);
    }

    /**
     * is basically a wrapper for imagettftext() to add the ability to center/right-align text to a poin
     * the $align argument can be 'c' for center, 'r' for right align, or 'l' for left align (default)
     */
    private function writeAlignedTextToImage(&$img, $size, $angle, $x, $y, &$c, $font, $string, $align = 'l')
    {
        $box = imagettfbbox($size, $angle, $font, $string);
        $w = $box[2] - $box[0];
        $h = $box[3] - $box[1];
        switch (strtolower($align)) {
            case 'r':
                $x -= $w;
                $y -= $h;
                break;
            case 'c':
                $x -= $w / 2;
                $y -= $h / 2;
                break;
        }
        imagettftext($img, $size, $angle, $x, $y, $c, $font, $string);
    }

    /**
     * downloads the content at $url and returns the raw string, not including the http header
     */
    private function download($url)
    {
        $results = null;
        if (function_exists('curl_init')) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Taiga');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $results = curl_exec($ch); // download!
            curl_close($ch);
        }
        if ($results === null) $results = file_get_contents($url); // curl failed, try url_fopen
        if ($results === null) die("Could not download from $url"); // give up
        return $results;
    }

    /**
     * @return MalData
     */
    public function getMalData(): MalData
    {
        return $this->malData;
    }
}
