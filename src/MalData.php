<?php

namespace WoodenDoors\Mal;

class MalData
{
    private $titles;
    private $status;
    private $current;
    private $totals;
    private $units;
    private $timestamps;
    private $types;
    private $subtypes;
    private $ids;

    /**
     * MalData constructor.
     * @param $titles
     * @param $status
     * @param $current
     * @param $totals
     * @param $units
     * @param $timestamps
     * @param $types
     * @param $subtypes
     * @param $ids
     */
    public function __construct($titles, $status, $current, $totals, $units, $timestamps, $types, $subtypes, $ids)
    {
        $this->titles = $titles;
        $this->status = $status;
        $this->current = $current;
        $this->totals = $totals;
        $this->units = $units;
        $this->timestamps = $timestamps;
        $this->types = $types;
        $this->subtypes = $subtypes;
        $this->ids = $ids;
    }


    /**
     * @return mixed
     */
    public function getTitles()
    {
        return $this->titles;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @return mixed
     */
    public function getTotals()
    {
        return $this->totals;
    }

    /**
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @return mixed
     */
    public function getTimestamps()
    {
        return $this->timestamps;
    }

    /**
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return mixed
     */
    public function getSubtypes()
    {
        return $this->subtypes;
    }

    /**
     * @return mixed
     */
    public function getIds()
    {
        return $this->ids;
    } // $ids is now an array containing the unique numeric mal series id



}