<?php

namespace WoodenDoors\Mal;

class SignatureText
{
    private $red;
    private $green;
    private $blue;
    private $alpha;
    private $size;
    private $angle;
    private $x;
    private $y;
    private $font;
    private $text;
    private $align;

    /**
     * SignatureText constructor.
     * @param $red
     * @param $green
     * @param $blue
     * @param $alpha
     * @param $size
     * @param $angle
     * @param $x
     * @param $y
     * @param $font
     * @param $text
     * @param $align
     */
    public function __construct(int $red, int $green, int $blue, int $alpha, int $size, int $angle, int $x, int $y,
                                string $font, string $text, string $align)
    {
        $this->red = $red;
        $this->green = $green;
        $this->blue = $blue;
        $this->alpha = $alpha;
        $this->size = $size;
        $this->angle = $angle;
        $this->x = $x;
        $this->y = $y;
        $this->font = $font;
        $this->text = $text;
        $this->align = $align;
    }

    /**
     * @return int
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * @return int
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * @return int
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * @return int
     */
    public function getAlpha(): int
    {
        return $this->alpha;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getAngle(): int
    {
        return $this->angle;
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return string
     */
    public function getFont(): string
    {
        return $this->font;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getAlign(): string
    {
        return $this->align;
    }
}
