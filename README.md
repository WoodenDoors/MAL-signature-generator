# MAL signature generator
* https://gitlab.com/WoodenDoors/MAL-signature-generator
* based on saka's minimal signature script
* resources are not included on purpose

## how
* install php7.1 and a few extensions (check composer.json)
* `composer install --no-dev` (might, rightfully, complain about missing extensions, use your package manager to get those)
* `chown -R myuser:www-data cache/`
* `chmod g+w www-data- cache/`
* if you run php-fpm under nginx, you can also use this for rewriting file ext:

```
rewrite ^/script/smss/sig/(.*).png$ /script/smss/$1/index.php;
rewrite ^/script/smss/sig_animated/(.*).gif$ /script/smss/$1/index.php;
```
